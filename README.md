# Crypto++

Crypto++ is a free C++ class library of cryptographic schemes https://cryptopp.com.

The source code for the library is located at Wei Dai's repo at https://github.com/weidai11/cryptopp. The project name on GitLab was taken to avoid name squatting and user confusion.
